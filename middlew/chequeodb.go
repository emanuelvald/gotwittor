package middlew

import (
	"net/http"
	"bitbucket.org/emanuelvald/gotwittor/bd"
)

/*
ChequeoDB es el middlew que permite conocer el estado de la base de datos
*/
func ChequeoDB(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if bd.ChequeoConecction() == 0 {
			http.Error(w, "Conexion perdida con la base de datos", http.StatusInternalServerError)
			return
		}
		next.ServeHTTP(w, r)
	}
}