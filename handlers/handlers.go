package handlers

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/emanuelvald/gotwittor/middlew"
	"bitbucket.org/emanuelvald/gotwittor/routers"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

/*
Permite dejar listo el puerto, handler y ejecuta el servidor.
*/
func Manejadores() {
	router := mux.NewRouter()
	PORT := os.Getenv("PORT")

	router.HandleFunc("/registro", middlew.ChequeoDB(routers.Registro)).Methods("POST")

	if PORT == "" {
		PORT = "8000"
	}

	handler := cors.AllowAll().Handler(router)

	log.Fatal(http.ListenAndServe(":" + PORT, handler))
}