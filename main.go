package main

import (
	"log"
	"bitbucket.org/emanuelvald/gotwittor/bd"
	"bitbucket.org/emanuelvald/gotwittor/handlers"
)
func main() {
	if bd.ChequeoConecction() == 0 {
		log.Fatal("Sin conexion a la base de datos")
		return
	}
	handlers.Manejadores()
}