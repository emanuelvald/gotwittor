package routers

import (
	"encoding/json"
	"net/http"
	"bitbucket.org/emanuelvald/gotwittor/bd"
	"bitbucket.org/emanuelvald/gotwittor/models"
)

/*
Funcion para crear un registro de usuario en la base de datos
*/
func Registro(w http.ResponseWriter, r *http.Request) {
	var t models.Usuario

	// el body de un http.Request es un string que se puede oir una vez
	err := json.NewDecoder(r.Body).Decode(&t)

	if err != nil {
		http.Error(w, "Error en los datos recibidos"+err.Error(), 400)
		return
	}
	if len(t.Email) == 0 {
		http.Error(w, "El email de usuario es requerido", 400)
	}
	if len(t.Password) <= 6 {
		http.Error(w, "El password debe ser mayor a 6 caracteres", 400)
	}

	// Los valores que no se necesitan usar, se puede omitir con un guion bajo
	_, encontrado, _ := bd.ChequeoYaExisteUsuario(t.Email)

	if encontrado == true {
		http.Error(w, "Email ya se encuentra en uso", 400)
	}

	_, status, err := bd.InsertoRegistro(t)

	if err != nil {
		http.Error(w, "Ocurrió un error al registrar usuario"+err.Error(), 400)
	}

	if status == false {
		http.Error(w, "No se ha logrado insertar un nuevo usuario", 400)
	}

	w.WriteHeader(http.StatusCreated)
}