package bd

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

/*
Permite conectar base de datos en la aplicacion.
*/
func ConectarBD() *mongo.Client {
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err.Error())
		return client
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err.Error())
		return client
	}

	log.Println("Conexion a base de datos exitosa")
	return client
}

/*
Ejecuta una prueba de ping a la base de datos conectada.
*/
func ChequeoConecction() int {
	err := MongoCN.Ping(context.TODO(), nil)

	if err != nil {
		return 0
	}
	return 1
}

/*
MongoCN: objecto de conexion a la base de datos.
*/
var MongoCN = ConectarBD()
var clientOptions = options.Client().ApplyURI("mongodb+srv://root:root@cluster0-7sdc4.mongodb.net/gotwittor?retryWrites=true&w=majority")