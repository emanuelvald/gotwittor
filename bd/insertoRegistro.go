package bd

import (
	"context"
	"time"
	"bitbucket.org/emanuelvald/gotwittor/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

/*
Inserta un nuevo registro de usuario en la base de datos
*/
func InsertoRegistro(u models.Usuario) (string, bool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15 * time.Second)

	// Ejecuta operaciones y cuando termine ejecuta el cancel
	defer cancel()

	db := MongoCN.Database("gotwittor")
	col := db.Collection("usuarios")

	u.Password, _ = EncriptarPassword(u.Password)

	result, err := col.InsertOne(ctx, u)

	if err != nil {
		return "", false, err
	}

	ObjID, _ := result.InsertedID.(primitive.ObjectID)

	return ObjID.String(), true, nil
}