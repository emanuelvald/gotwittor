package bd

import (
	"golang.org/x/crypto/bcrypt"
)

/*
Permite la increiptacion de contraseña.
*/
func EncriptarPassword(pass string) (string, error) {
	// Algoritmo de de 2 elevado en 8. Cuando se encripta un usuario
	// se utiliza 6, y cuando se encripta un superusuario 8.
	costo := 8

	bytes, err := bcrypt.GenerateFromPassword([]byte(pass), costo)

	return string(bytes), err
}