package bd

import (
	"context"
	"time"

	"bitbucket.org/emanuelvald/gotwittor/models"
	"go.mongodb.org/mongo-driver/bson"
)

/*
Verifica si existe usuario verificando email en base de datos.
*/
func ChequeoYaExisteUsuario(email string) (models.Usuario, bool, string) {
	ctx, cancel := context.WithTimeout(context.Background(), 15 * time.Second)

	defer cancel()

	db := MongoCN.Database("gotwittor")
	col := db.Collection("usuarios")

	condicionBusqueda := bson.M{"email": email}

	var resultados models.Usuario

	err := col.FindOne(ctx, condicionBusqueda).Decode(&resultados)
	ID := resultados.Id.Hex()

	if err != nil {
		return resultados, false, ID
	}

	return resultados, true, ID
}